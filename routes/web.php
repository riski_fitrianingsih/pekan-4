<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect(route('login'));
});

Route::group(['middleware' => 'auth'], function() {

    // CERITA
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/cerita', 'CrudController@index')->name('cerita.create');
    Route::post('/cerita', 'CrudController@store')->name('store');
    Route::get('/cerita/{notes}/edit', 'CrudController@edit')->name('edit');
    Route::patch('/cerita/update/{notes}', 'CrudController@update')->name('update');
    Route::get('/cerita/show/{notes}', 'CrudController@show')->name('show');
    Route::delete('/cerita/{notes}', 'CrudController@destroy')->name('cerita.delete');
    
    // CRUD
    Route::get('/kopit', 'CrudController@create')->name('jancuker');

  // Profile
    Route::get('profile', 'Admin\UserController@profile')->name('profile');
    Route::post('profile', 'Admin\UserController@update')->name('updateProfile');
    Route::post('profile/image', 'Admin\UserController@ubahfoto')->name('ubahFoto');
    Route::patch('profile/password', 'Admin\UserController@ubahPassword')->name('ubahPassword');

  // Manajemen User
  Route::get('/manajemen_user', 'UserController@index')->name('manajemen_user.index');
  Route::post('/manajemen_user', 'UserController@store')->name('manajemen_user.store');
  Route::get('/manajemen_user/{manajemen_user}/edit', 'UserController@edit')->name('edit');
  Route::patch('/manajemen_user/update/{manajemen_user}', 'UserController@update')->name('update');
  Route::get('/manajemen_user/show/{manajemen_user}', 'UserController@show')->name('show');
  Route::delete('/manajemen_user/{manajemen_user}', 'UserController@destroy')->name('manajemen_user.delete');
});



