 <div class="row">
     <div class="col-md-6">
         <div class="form-group">
          <input type="hidden" value="{{ $note->id }}" id="id_data"/>
             <label>Title</label>
             <input id="title" name="title" placeholder="Masukkan Title" type="text" class="form-control"
                 value="{{$note->title}}" required>
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Deskripsi</label>
             <input id="description" name="description" placeholder="Masukkan Deskripsi" type="text"
                 class="form-control" value="{{$note->description}}" required>
         </div>
     </div>